const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utils/errorResponse')
const Groups = require('../models/Groups')

// @desc    Get all groups
// @route   GET /v1/groups
// @access  Public
exports.getGroups = asyncHandler(async (req, res, next) => {
  let query
  const reqQuery = { ...req.query }
  const removeFields = ['select', 'sort', 'page', 'limit']
  removeFields.forEach((param) => delete reqQuery[param])

  let queryStr = JSON.stringify(reqQuery)

  queryStr = queryStr.replace(/\b(gt|gte|lt|lte|in)\b/g, (match) => `$${match}`)

  query = Groups.find(JSON.parse(queryStr)).populate('posts')

  if (req.query.select) {
    const fields = req.query.select.split(',').join(' ')
    query = query.select(fields)
  }

  if (req.query.sort) {
    const sortBy = req.query.sort.split(',').join(' ')
    query = query.sort(sortBy)
  } else {
    query = query.sort('-createdAt')
  }

  // Pagination
  const page = parseInt(req.query.page, 10) || 1
  const limit = parseInt(req.query.limit, 10) || 100
  const startIndex = (page - 1) * limit
  const endIndex = page * limit
  const total = await Groups.countDocuments()

  query = query.skip(startIndex).limit(limit)

  // Executing query
  const groups = await query

  // Pagination result
  const pagination = {}
  if (endIndex < total) {
    pagination.next = {
      page: page + 1,
      limit,
    }
  }
  if (startIndex > 0) {
    pagination.prev = {
      page: page - 1,
      limit,
    }
  }

  res
    .status(200)
    .json({ success: true, pagination, count: total, data: groups })
})

// @desc    Get signle group
// @route   GET /v1/group/:id
// @access  Public
exports.getGroup = asyncHandler(async (req, res, next) => {
  res.status(200).json({ success: true, data: 'get single group' })
})

// @desc    Create new Group
// @route   POST /v1/group
// @access  Private/Superadmin
exports.createGroup = asyncHandler(async (req, res, next) => {
  const group = await Group.create(req.body)
  res.status(201).json({ success: true, data: group })
})

// @desc    Add courses to group

// @desc    Update group , add members to group
// @route   PUT /v1/group/:id
// @access  Private/Superadmin
exports.updateGroup = asyncHandler(async (req, res, next) => {
  let group = await Groups.findById(req.params.id)
  if (!group) {
    return next(new ErrorResponse(`No group with id ${req.params.id}`, 404))
  }
  group = await Groups.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  })
  res.status(200).json({ success: true, data: group })
})

exports.deleteGroup = asyncHandler(async (req, res, next) => {
  const group = await Groups.findById(req.params.id)

  if (!group) {
    return next(
      new ErrorResponse(`Group not found with id of ${req.params.id}`, 404)
    )
  }

  await group.remove()

  res.status(200).json({ success: true, data: {} })
})
