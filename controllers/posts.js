const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utils/errorResponse')
const Posts = require('../models/Posts')

// @desc    Create new Post
// @route   POST /v1/posts
// @access  Private (user, admin, superadmin)
exports.createPost = asyncHandler(async (req, res, next) => {
  const post = await Posts.create(req.body)
  res.status(201).json({ success: true, data: post })
})

// @desc    Update post
// @route   PUT /v1/posts/:id
// @access  Private
exports.updatePost = asyncHandler(async (req, res, next) => {
  const post = await Posts.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  })
  if (!post) throw new Error('couldnot find id')
  res.status(200).json({ data: post })
})

// @desc    Get all posts
// @route   GET /v1/posts
// @route   GET /v1/user/:userId/posts
// @route   GET /v1/group/:groupId/posts
// @access  Public
exports.getPosts = asyncHandler(async (req, res, next) => {
  let query
  if (req.params.userId) {
    query = Posts.find({ user: req.params.userId })
  } else if (req.params.groupId) {
    query = Posts.find({ group: req.params.groupId })
  } else {
    query = Posts.find()
      .populate({
        path: 'user',
        select: 'name role',
      })
      .populate('group')
  }

  const posts = await query
  // const posts = await Posts.find()
  res.status(200).json({ succss: true, data: posts })
})

// @desc    Get single posts
// @route   GET /v1/posts/:id
// @access  Private
exports.getPost = asyncHandler(async (req, res, next) => {
  const post = await (await Posts.findById(req.params.id))
    .populated('user')
    .populate('group')

  if (!post) {
    return next(new ErrorResponse(`No post with id of ${req.params.id}`, 404))
  }
  res.status(200).json({ success: true, data: post })
})
