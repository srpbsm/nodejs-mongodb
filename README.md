# Nodejs-MongoDB

## Usage

Rename "config/config.env.env" to "config/config.env" and update the values/settings to your own.
If using vs-code and prettier. Go to prettier manage and tick Prettier:Require Config.

## Install Dependencies

```
npm install
```

## Setup MongoDB

mongodb in docker container

```
docker run -it -p 27017:27017 --name mongoContainer mongo:latest mongo
```

Otherwise, if your container is already running, you can use the exec command:

```
docker exec -it mongoContainer mongo
```

## Run App

```
npm start
```

## Testing

Docker must be installed

```
docker-compose up --build --abort-on-container-exit
```

## Docker ( usefull commands )

```
docker images
docker build -t your_dockerhub_username/nodejs-image-demo .
docker run --name nodejs-image-demo -p 80:8080 -d your_dockerhub_username/nodejs-image-demo
docker ps

docker login -u your_dockerhub_username
docker push your_dockerhub_username/nodejs-image-demo

> --env-file <location of file>

docker system prune -a
```
