const mongoose = require('mongoose')
const User = require('./User')

const GroupSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please add a name'],
      trim: true,
      maxlength: [100, 'Name can not be more than 100 character'],
    },
    description: {
      type: String,
      required: [true, 'Please add a description'],
      trim: true,
      maxlength: [500, 'Name can not be more than 500 character'],
    },
    members: [
      {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
      },
    ],
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
)

// Cascade delete posts when group is deleted
GroupSchema.pre('remove', async function (next) {
  await this.model('Posts').deleteMany({ group: this._id })
  next()
})

// Reverse populate with virtuals
GroupSchema.virtual('posts', {
  ref: 'Posts',
  localField: '_id',
  foreignField: 'group',
  justOne: false,
})

module.exports = mongoose.model('Group', GroupSchema)
