const mongoose = require('mongoose')

const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Please add a name'],
    trim: true,
    maxlength: [100, 'Name can not be more than 100 character'],
  },
  description: {
    type: String,
    trim: true,
    maxlength: [500, 'Name can not be more than 500 character'],
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: true,
  },
  group: {
    type: mongoose.Schema.ObjectId,
    ref: 'Group',
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
})

// Static method to get total
PostSchema.statics.getTotalPosts = async function (groupId) {
  const obj = await this.aggregate([
    {
      $match: { group: groupId },
    },
    {
      $group: {
        _id: '$group',
        count: { $sum: 1 },
      },
    },
  ])
  console.log(obj)

  try {
    await this.model('Group').findByIdAndUpdate(groupId, {
      total: obj[0].count,
    })
  } catch (err) {
    console.log(err)
  }
}

// Call getTotal after save
PostSchema.post('save', function () {
  this.constructor.getTotalPosts(this.group)
})

// Call getTotal before remove
PostSchema.pre('save', function () {
  this.constructor.getTotalPosts(this.group)
})

module.exports = mongoose.model('Post', PostSchema)
