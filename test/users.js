const chai = require('chai')
const chaiHttp = require('chai-http')
const request = require('request')

const server = process.env.APP_URL || require('../server')

// Assertion style
chai.should()
chai.use(chaiHttp)

describe('Users API', () => {
  /**
   * Test the GET rout
   */
  describe('GET /v1/users', () => {
    it('should get all the users', (done) => {
      chai
        .request(server)
        .get('/v1/users')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('data')
          done()
        })
    })

    // error handling
  })
  /**
   * Test the GET by id route
   */
  describe('GET /v1/users/:id', () => {
    it('should get all the users', (done) => {
      const id = 'asdki'
      chai
        .request(server)
        .get('/v1/users/' + id)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('data')
          done()
        })

      // error handling
    })
  })

  /**
   * Test the POST route
   */
  describe('POST /v1/users', () => {
    it('should post new users', (done) => {
      const user = {
        email: 'bikesh@gmail.com',
        name: 'bikesh shrestha',
        password: 'bikeiksie',
      }
      chai
        .request(server)
        .post('/v1/users')
        .send(user)
        .end((err, res) => {
          res.should.have.status(201)
          res.body.should.be.a('object')
          res.body.should.have.property('data')
          done()
        })
    })
  })

  /**
   * Test the DELETE route
   */
})
