const chai = require('chai')
const chaiHttp = require('chai-http')
const request = require('request')

const server = process.env.APP_URL || require('../server')

// Assertion style
chai.should()
chai.use(chaiHttp)

describe('Groups API', () => {
  /**
   * Test the GET rout
   */
  describe('GET /v1/groups', () => {
    it('should get all the groups', (done) => {
      chai
        .request(server)
        .get('/v1/groups')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('data')
          done()
        })
    })

    // error handling
  })
  /**
   * Test the GET by id route
   */

  /**
   * Test the POST route
   */

  /**
   * Test the DELETE route
   */
})
