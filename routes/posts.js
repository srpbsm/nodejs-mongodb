const role = require('../_helpers/role')
const { protect, authorize } = require('../middleware/auth')
const express = require('express')
const {
  getPosts,
  getPost,
  updatePost,
  createPost,
} = require('../controllers/posts')
const router = express.Router({ mergeParams: true })

router.route('/').get(getPosts).post(protect, createPost)
router
  .route('/:id')
  .get(getPost)
  .put(protect, authorize(role.Superadmin, role.Admin), updatePost)

module.exports = router
