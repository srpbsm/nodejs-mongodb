const express = require('express')
const { protect, authorize } = require('../middleware/auth')
const {
  getUser,
  getUsers,
  createUser,
  updateUser,
  deleteUser,
} = require('../controllers/users')

// Include other resource routers
const postsRouter = require('./posts')

const router = express.Router()

// Re-route into other resource routers
router.use('/:userId/posts', postsRouter)

router.use(protect)
router.use(authorize('admin', 'user'))

router.get('/', getUsers)
router.get('/:id', getUser)
router.post('/craeteuser', createUser)
router.put('/updateuser/:id', updateUser)
router.put('/deleteUser/:id', deleteUser)

module.exports = router
