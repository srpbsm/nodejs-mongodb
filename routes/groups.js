const role = require('../_helpers/role')
const { protect, authorize } = require('../middleware/auth')
const express = require('express')
const {
  getGroup,
  getGroups,
  updateGroup,
  createGroup,
  deleteGroup,
} = require('../controllers/groups')

// Include other resource routers
const postsRouter = require('./posts')

const router = express.Router()

// Re-route into other resource routers
router.use('/:groupId/posts', postsRouter)

router
  .route('/')
  .get(getGroups)
  .post(protect, authorize(role.Superadmin), createGroup)
router
  .route('/:id')
  .get(getGroup)
  .put(protect, authorize(role.Superadmin), updateGroup)
  .delete(protect, authorize(role.Superadmin), deleteGroup)

module.exports = router
